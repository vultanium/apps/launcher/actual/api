package fr.shaiwen.vultanium;

public enum Domain {

    HTTP("http"),
    HTTPS("https");

    private String domain;

    private Domain(String domain) {
        this.domain = domain;
    }

    public String toString() {
        return this.domain;
    }

    public String getDomain() {
        return this.domain;
    }
}
